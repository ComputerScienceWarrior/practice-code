﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_30_2019
{
	class Program
	{
		static void Main(string[] args)
		{
			Greeting();//greeting code

			Console.WriteLine("Enter a number");
			int number1 = int.Parse(Console.ReadLine());
			Console.WriteLine("Enter another number");
			int number2 = int.Parse(Console.ReadLine());

			int total = 0;//accumulator value

			int[] numsArray = new int[] { number1, number2, 3, 4, 5 }; // create an array with user values and other values to total 5 entries

			/*
			for(int i = 0; i < numsArray.Length; i++)
			{
					total += numsArray[i];
					Console.WriteLine("The current total for index " + i + " is: " + total);
			}
			*/

			//copy array values
			int[] numsCopy = new int[numsArray.Length];//make array the same size as array we're copying
			
			if(numsCopy.Length != numsArray.Length)
			{
				Console.WriteLine("These arrays will not copy properly.");
			}
			else
			{
				for(int i = 0; i < numsArray.Length; i++)
				{
					numsCopy[i] = numsArray[i];
				}
			}

			//display numsCopy values
			Console.WriteLine("Values for numsCopy array: ");
			foreach(int num in numsCopy)
			{
				Console.Write(num + " ");
			}
			Console.WriteLine("Values for numsArray: ");
			foreach(int num in numsArray)
			{
				Console.Write(num + " ");
			}


		}

		public static void Greeting()
		{
			Console.WriteLine("Welcome to coding practice, Computer Science Warrior.");
			Console.WriteLine("Show me what you've got!");
		}
	}
}
